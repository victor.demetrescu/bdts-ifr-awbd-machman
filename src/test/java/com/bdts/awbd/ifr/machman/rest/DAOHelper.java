package com.bdts.awbd.ifr.machman.rest;

import com.bdts.awbd.ifr.machman.dao.LanDAO;
import com.bdts.awbd.ifr.machman.dao.UserDAO;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.r2dbc.R2dbcDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.r2dbc.R2dbcRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.r2dbc.R2dbcAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@DataR2dbcTest
public class DAOHelper {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private LanDAO lanDAO;
}
