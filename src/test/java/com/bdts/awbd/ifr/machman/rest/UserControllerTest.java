package com.bdts.awbd.ifr.machman.rest;

import com.bdts.awbd.ifr.machman.dao.UserDAO;
import com.bdts.awbd.ifr.machman.entity.User;
import com.bdts.awbd.ifr.machman.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveSecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = UserController.class, excludeAutoConfiguration = ReactiveSecurityAutoConfiguration.class)
@Import({UserServiceImpl.class, DAOHelper.class})
public class UserControllerTest {

    private static final String USER_BY_NAME_PATH = "/user/by-name/%s";

    @Value("${test.username}")
    private String username;

    @Value("${test.password}")
    private String password;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    UserDAO userDAO;

    @Test
    public void getUserByUsernameWithNoCredentialsTest() {
        webTestClient
                .get()
                .uri(getUserByNameURI())
                .exchange()
                .expectStatus()
                .isUnauthorized()
                .expectBody()
                .isEmpty();
    }

    @Test
    public void getUserByUsernameTest() {
        webTestClient
                .get()
                .uri(getUserByNameURI())
                .headers(httpHeaders -> httpHeaders.setBasicAuth(username, password))
                .exchange()
                .expectStatus()
                .isOk();
    }

    private String getUserByNameURI() {
        return String.format(USER_BY_NAME_PATH, username);
    }

    private User testUser() {
        return User
                .builder()
                .id(1L)
                .username(username)
                .password(new BCryptPasswordEncoder().encode(password))
                .role("USER")
                .active(true)
                .build();
    }
}
