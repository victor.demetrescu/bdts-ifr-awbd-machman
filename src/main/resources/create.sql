create table CLUSTERS
(
    ID bigint auto_increment
        primary key,
    NAME varchar(255) not null,
    LAN_ID bigint not null,
    constraint CLUSTERS_FK_LAN_ID
        unique (LAN_ID),
    constraint CLUSTERS_name_uindex
        unique (NAME)
);

create table DEPENDENCIES
(
    ID bigint auto_increment
        primary key,
    NAME varchar(255) not null,
    VERSION varchar(255) not null
);

create table LANS
(
    ID bigint auto_increment
        primary key,
    NAME varchar(255) not null,
    constraint LANS_name_uindex
        unique (NAME)
);

create table MACHINES
(
    ID bigint auto_increment
        primary key,
    HOSTNAME varchar(255) not null,
    IP varchar(255) not null,
    CLUSTER_ID bigint not null,
    MACHINE_SPECIFICATION_ID bigint null,
    constraint MACHINES_HOSTNAME_uindex
        unique (HOSTNAME),
    constraint MACHINES_IP_uindex
        unique (IP),
    constraint MACHINES_CLUSTERS_ID_fk
        foreign key (CLUSTER_ID) references CLUSTERS (ID)
);

create table MACHINE_DEPENDENCIES
(
    ID bigint auto_increment,
    MACHINE_ID bigint not null,
    DEPENDENCY_ID bigint not null,
    constraint MACHINE_DEPENDENCIES_ID_uindex
        unique (ID),
    constraint MACH_DEP_DEPENDENCIES_ID_fk
        foreign key (DEPENDENCY_ID) references DEPENDENCIES (ID),
    constraint MCH_DEP_MACHINES_ID_fk
        foreign key (MACHINE_ID) references MACHINES (ID)
);

alter table MACHINE_DEPENDENCIES
    add primary key (ID);

create table MACHINE_SPECIFICATIONS
(
    ID bigint auto_increment
        primary key,
    TYPE varchar(255) not null,
    RAM_UNIT varchar(255) not null,
    RAM_VALUE double not null,
    CPU_NAME varchar(255) not null,
    CPU_SERIAL_CODE varchar(255) not null,
    CPU_CORES int not null,
    CPU_FREQUENCY double not null,
    MACHINE_ID bigint not null,
    constraint MACHINE_SPECIFICATIONS_MACHINES_ID_fk
        foreign key (MACHINE_ID) references MACHINES (ID)
);

alter table MACHINES
    add constraint MACHINES_MACHINE_SPECIFICATIONS_ID_fk
        foreign key (MACHINE_SPECIFICATION_ID) references MACHINE_SPECIFICATIONS (ID);

create table USERS
(
    ID bigint auto_increment
        primary key,
    USERNAME varchar(255) not null,
    PASSWORD varchar(255) not null,
    ROLE varchar(255) not null,
    constraint UK_USERNAME
        unique (USERNAME)
);