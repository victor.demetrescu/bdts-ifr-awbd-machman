package com.bdts.awbd.ifr.machman.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import java.util.Set;

@Table("CLUSTERS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Cluster {

    @Id
    @Column("ID")
    private Long id;

    @Column("NAME")
    @org.springframework.lang.NonNull
    private String name;

    @Column("LAN_ID")
    @org.springframework.lang.NonNull
    @JsonIgnore
    private Long lanId;

    @Transient
    private Lan lan;

    @MappedCollection(idColumn = "CLUSTER_ID")
    private Set<Machine> machines;
}
