package com.bdts.awbd.ifr.machman.config.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
public class HttpLogFilter implements WebFilter {

    private static final Logger log = LoggerFactory.getLogger(HttpLogFilter.class);

    @Override
    public Mono<Void> filter(final ServerWebExchange serverWebExchange,
                             final WebFilterChain webFilterChain) {

        return serverWebExchange
                .getPrincipal()
                .flatMap(principal -> {
                    final String username = principal.getName();
                    final String method = serverWebExchange.getRequest().getMethod().name();
                    final String path = serverWebExchange.getRequest().getPath().value();
                    final HttpStatus responseStatus = serverWebExchange.getResponse().getStatusCode();

                    log.info("{} -> [{}] {} [{} {}]", username, method, path, responseStatus.value(), responseStatus.getReasonPhrase());

                    return webFilterChain.filter(serverWebExchange);
                });
    }
}
