package com.bdts.awbd.ifr.machman.dao.converter.read;

import com.bdts.awbd.ifr.machman.entity.Cluster;
import io.r2dbc.spi.Row;
import org.mariadb.r2dbc.MariadbRow;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import static com.bdts.awbd.ifr.machman.dao.converter.read.ReadConverterHelper.getLan;

@ReadingConverter
public class ClusterReadConverter implements Converter<Row, Cluster> {

    @Override
    public Cluster convert(final Row source) {
        final MariadbRow row = (MariadbRow) source;

        return Cluster
                .builder()
                .id(row.get("ID", Long.class))
                .name(row.get("NAME", String.class))
                .lanId(row.get("LAN_ID", Long.class))
                .lan(getLan(row))
                .build();
    }
}
