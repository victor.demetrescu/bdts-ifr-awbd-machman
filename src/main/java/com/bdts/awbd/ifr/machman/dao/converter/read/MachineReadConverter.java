package com.bdts.awbd.ifr.machman.dao.converter.read;

import com.bdts.awbd.ifr.machman.entity.Machine;
import io.r2dbc.spi.Row;
import org.mariadb.r2dbc.MariadbRow;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import static com.bdts.awbd.ifr.machman.dao.converter.read.ReadConverterHelper.getCluster;
import static com.bdts.awbd.ifr.machman.dao.converter.read.ReadConverterHelper.getMachineSpecification;

@ReadingConverter
public class MachineReadConverter implements Converter<Row, Machine> {

    @Override
    public Machine convert(final Row source) {
        final MariadbRow row = (MariadbRow) source;

        return Machine
                .builder()
                .id(row.get("ID", Long.class))
                .hostname(row.get("HOSTNAME", String.class))
                .ip(row.get("IP", String.class))
                .specificationId(row.get("MACHINE_SPECIFICATION_ID", Long.class))
                .clusterId(row.get("CLUSTER_ID", Long.class))
                .cluster(getCluster(row))
                .machineSpecification(getMachineSpecification(row))
                .build();
    }

}
