package com.bdts.awbd.ifr.machman.dao.custom.base.crud;

import org.springframework.r2dbc.core.DatabaseClient;
import reactor.core.publisher.Mono;

import java.util.Map;

public interface BaseCustomCreateDAO extends BaseCustomDAO {

    default Mono<Void> persist(final String query,
                               final Map<String, Object> paramBinding) {
        final DatabaseClient.GenericExecuteSpec sql = getDatabaseClient().sql(query);

        paramBinding.forEach(sql::bind);

        return sql.then();

    }
}
