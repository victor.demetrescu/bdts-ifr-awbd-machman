package com.bdts.awbd.ifr.machman.rest;

import com.bdts.awbd.ifr.machman.entity.User;
import com.bdts.awbd.ifr.machman.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/user")
@PreAuthorize("isAuthenticated()")
public class UserController extends BaseController {

    private final UserService userService;

    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    public Flux<User> getAllUsers() {
        return userService.findAll();
    }

    @GetMapping("/by-name/{username}")
    public Mono<UserDetails> getUserByUsername(@PathVariable("username") final String username) {
        return userService.findByUsername(username);
    }

}
