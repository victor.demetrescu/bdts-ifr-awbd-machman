package com.bdts.awbd.ifr.machman.rest;

import com.bdts.awbd.ifr.machman.entity.Lan;
import com.bdts.awbd.ifr.machman.service.LanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/lan")
@PreAuthorize("isAuthenticated()")
public class LanController extends BaseController {

    private final LanService lanService;

    @Autowired
    public LanController(final LanService lanService) {
        this.lanService = lanService;
    }

    @GetMapping("/{id}")
    public Mono<Lan> getLanById(@PathVariable("id") final Long id) {
        return lanService.findById(id);
    }

    @GetMapping("/all")
    public Flux<Lan> getAllLans() {
        return lanService.findAll();
    }

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Lan> addDependency(@RequestBody final Lan lan) {
        return lanService.save(lan);
    }

    @DeleteMapping("/delete/{id}")
    public Mono<Void> deleteLanById(@PathVariable("id") final Long id) {
        return lanService.deleteById(id);
    }
}
