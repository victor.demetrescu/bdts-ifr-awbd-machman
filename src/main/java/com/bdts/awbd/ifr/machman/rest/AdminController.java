package com.bdts.awbd.ifr.machman.rest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/admin")
@PreAuthorize("isAuthenticated() and hasRole('ADMIN')")
public class AdminController {

    @GetMapping("/heartbeat")
    public Mono<String> getHeartbeat() {
        return Mono.just("OK");
    }
}
