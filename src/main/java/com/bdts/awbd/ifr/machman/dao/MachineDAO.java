package com.bdts.awbd.ifr.machman.dao;

import com.bdts.awbd.ifr.machman.entity.Machine;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface MachineDAO extends BaseDAO, ReactiveSortingRepository<Machine, Long> {

    String baseQuery = "SELECT m.*, ";
    String baseFrom = "FROM MACHINES m ";
    String whereClauseById = "WHERE m.id = :id ";

    String clusterQuery = "c.ID AS CLUSTER_ID, c.NAME AS CLUSTER_NAME, c.LAN_ID AS CLUSTER_LAN_ID ";
    String clusterJoin = "JOIN CLUSTERS c on c.id = m.CLUSTER_ID ";

    String machineSpecificationQuery = "ms.ID AS MACHINE_SPEC_ID, ms.TYPE AS MACHINE_SPEC_TYPE, ms.RAM_UNIT AS MACHINE_SPEC_RAM_UNIT, " +
            "ms.RAM_VALUE AS MACHINE_SPEC_RAM_VALUE, ms.CPU_NAME AS MACHINE_SPEC_CPU_NAME, ms.CPU_SERIAL_CODE AS MACHINE_SPEC_CPU_SERIAL_CODE,  " +
            "ms.CPU_CORES AS MACHINE_SPEC_CPU_CORES, ms.CPU_FREQUENCY AS MACHINE_SPEC_CPU_FREQUENCY, ms.MACHINE_ID as MACHINE_ID ";
    String machineSpecificationJoin = "JOIN MACHINE_SPECIFICATIONS ms on ms.ID = m.MACHINE_SPECIFICATION_ID ";

    @Query(baseQuery + clusterQuery + baseFrom + clusterJoin + whereClauseById)
    Mono<Machine> findByIdWithCluster(final Long id);


    @Query(baseQuery + clusterQuery + queryParamsSeparator + machineSpecificationQuery + baseFrom + clusterJoin + machineSpecificationJoin + whereClauseById)
    Mono<Machine> findByIdWithClusterAndMachineSpecification(final Long id);
}
