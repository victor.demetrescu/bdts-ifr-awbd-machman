package com.bdts.awbd.ifr.machman.dao.converter.write;

import com.bdts.awbd.ifr.machman.entity.Lan;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.r2dbc.core.Parameter;

import static com.bdts.awbd.ifr.machman.dao.converter.write.WriteConverterHelper.addPrimaryKeyToRow;

@WritingConverter
public class LanWriteConverter implements Converter<Lan, OutboundRow> {

    @Override
    public OutboundRow convert(final Lan source) {
        final OutboundRow row = new OutboundRow();

        addPrimaryKeyToRow(source.getId(), row);
        row.put("NAME", Parameter.from(source.getName()));

        return row;
    }
}
