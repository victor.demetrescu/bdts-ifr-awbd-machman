package com.bdts.awbd.ifr.machman.dao.custom.base.crud;

import org.springframework.r2dbc.core.DatabaseClient;

public interface BaseCustomDAO {

    DatabaseClient getDatabaseClient();
}
