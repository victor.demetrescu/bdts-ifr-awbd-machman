package com.bdts.awbd.ifr.machman.dao.custom;

import com.bdts.awbd.ifr.machman.dao.custom.base.abstracts.AbstractBaseDAO;
import com.bdts.awbd.ifr.machman.entity.Dependency;
import com.bdts.awbd.ifr.machman.entity.Machine;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class DependencyReadDAO extends AbstractBaseDAO {

    private static final String ALL_DEPENDENCIES_QUERY = "SELECT " +
            "dependency.id AS ID, " +
            "dependency.NAME AS NAME, " +
            "dependency.VERSION AS VERSION, " +
            "machine.id AS MACHINE_ID, " +
            "machine.HOSTNAME AS MACHINE_HOSTNAME, " +
            "machine.IP AS MACHINE_IP, " +
            "machine.CLUSTER_ID AS CLUSTER_ID, " +
            "machine.MACHINE_SPECIFICATION_ID AS MACHINE_SPEC_ID " +
            "FROM DEPENDENCIES dependency " +
            "LEFT JOIN MACHINE_DEPENDENCIES md on md.DEPENDENCY_ID = dependency.ID " +
            "LEFT JOIN MACHINES machine on machine.ID = md.MACHINE_ID ";
    private static final String DEPENDENCY_BY_ID_QUERY = ALL_DEPENDENCIES_QUERY + "WHERE dependency.id = :ID ";
    private static final String ALL_DEPENDENCIES_FULL_QUERY = ALL_DEPENDENCIES_QUERY + ORDER_BY_PRIMARY_KEY;

    @SuppressWarnings("unchecked")
    public Mono<Dependency> findById(final Long id) {
        return (Mono<Dependency>) findById(id, DEPENDENCY_BY_ID_QUERY, this::convertEntityFromResults);
    }

    @SuppressWarnings("unchecked")
    public Flux<Dependency> findAll() {
        return (Flux<Dependency>) findAll(ALL_DEPENDENCIES_FULL_QUERY, this::convertEntityFromResults);

    }

    private Dependency convertEntityFromResults(final List<Map<String, Object>> results) {
        final Long dependencyId = getPrimaryKey(results);
        return Dependency
                .builder()
                .id(dependencyId)
                .name((String) getBaseProperty(results, "NAME"))
                .version((String) getBaseProperty(results, "VERSION"))
                .machines(getMachinesFromResults(results))
                .build();
    }

    private Set<Machine> getMachinesFromResults(final List<Map<String, Object>> results) {
        return results
                .stream()
                .map(row -> getMachineFromRow(row, false))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

}
