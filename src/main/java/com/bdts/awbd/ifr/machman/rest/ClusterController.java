package com.bdts.awbd.ifr.machman.rest;

import com.bdts.awbd.ifr.machman.entity.Cluster;
import com.bdts.awbd.ifr.machman.service.ClusterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/cluster")
@PreAuthorize("isAuthenticated()")
public class ClusterController extends BaseController {

    private final ClusterService clusterService;

    @Autowired
    public ClusterController(final ClusterService clusterService) {
        this.clusterService = clusterService;
    }

    @GetMapping("/all/query")
    public Flux<Cluster> getAllClustersUsingQuery() {
        return clusterService.findAllUsingQuery();
    }

    @GetMapping("/all/service")
    public Flux<Cluster> getAllClustersService() {
        return clusterService.findAllUsingService();
    }
}
