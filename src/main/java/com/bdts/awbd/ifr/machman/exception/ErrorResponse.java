package com.bdts.awbd.ifr.machman.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ErrorResponse {

    private final String message;
    private final int status;

    public static ErrorResponse fromException(final Throwable exception) {
        if (exception instanceof MachmanException) {
            return ErrorResponse.fromMachmanException((MachmanException) exception);
        }
        return new ErrorResponse(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    private static ErrorResponse fromMachmanException(final MachmanException machmanException) {
        return new ErrorResponse(machmanException.getErrorMessage(), machmanException.getStatusCode());
    }

    private ErrorResponse(final String message,
                         final int status) {
        this.message = message;
        this.status = status;
    }
}
