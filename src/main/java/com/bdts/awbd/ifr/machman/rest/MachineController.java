package com.bdts.awbd.ifr.machman.rest;

import com.bdts.awbd.ifr.machman.entity.Machine;
import com.bdts.awbd.ifr.machman.entity.MachineSpecification;
import com.bdts.awbd.ifr.machman.service.MachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/machine")
@PreAuthorize("isAuthenticated()")
public class MachineController extends BaseController {

    private final MachineService machineService;

    @Autowired
    public MachineController(final MachineService machineService) {
        this.machineService = machineService;
    }

    @GetMapping(value = {
            "/{id}",
            "/{id}/{withCluster}"
            , "/{id}/{withCluster}/{withMachineSpecification}"})
    public Mono<Machine> getMachineById(@PathVariable("id") final Long id,
                                        @PathVariable(value = "withCluster", required = false) final boolean withCluster,
                                        @PathVariable(value = "withMachineSpecification", required = false) final boolean withMachineSpecification) {
        return machineService.getMachineById(id, withCluster, withMachineSpecification);
    }

    @GetMapping(value = {
            "/specifications/{id}",
            "/specifications/{id}/{withMachine}"
            , "/specifications/{id}/{withMachine}/{withCluster}"})
    public Mono<MachineSpecification> getMachineSpecificationsById(@PathVariable("id") final Long id,
                                                                   @PathVariable(value = "withMachine", required = false) final boolean withMachine,
                                                                   @PathVariable(value = "withCluster", required = false) final boolean withCluster) {
        return machineService.getMachineSpecificationById(id, withMachine, withCluster);
    }

}
