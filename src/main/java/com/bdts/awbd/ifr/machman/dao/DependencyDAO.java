package com.bdts.awbd.ifr.machman.dao;

import com.bdts.awbd.ifr.machman.entity.Dependency;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DependencyDAO extends ReactiveSortingRepository<Dependency, Long> {
}
