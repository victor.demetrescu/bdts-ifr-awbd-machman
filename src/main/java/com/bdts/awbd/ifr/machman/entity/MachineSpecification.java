package com.bdts.awbd.ifr.machman.entity;

import com.bdts.awbd.ifr.machman.entity.embedded.CpuInfo;
import com.bdts.awbd.ifr.machman.entity.embedded.RamInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Embedded;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

@Table("MACHINE_SPECIFICATIONS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MachineSpecification {

    @Id
    @Column("ID")
    private Long id;

    @Column("TYPE")
    private MachineType type;

    @Embedded(prefix = "RAM", onEmpty = Embedded.OnEmpty.USE_NULL)
    @NonNull
    private RamInfo ram;


    @Embedded(prefix = "CPU", onEmpty = Embedded.OnEmpty.USE_NULL)
    @NonNull
    private CpuInfo cpuInfo;

    @Column("MACHINE_ID")
    @NonNull
    @JsonIgnore
    private Long machineId;

    @Transient
    private Machine machine;
}
