package com.bdts.awbd.ifr.machman.entity.embedded;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.lang.NonNull;

@Data
@Builder
public class RamInfo {

    @Column("UNIT")
    @NonNull
    private String unit;

    @Column("VALUE")
    @NonNull
    private Double value;
}
