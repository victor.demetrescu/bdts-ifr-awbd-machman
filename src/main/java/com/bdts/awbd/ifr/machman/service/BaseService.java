package com.bdts.awbd.ifr.machman.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BaseService<T, ID> {

    Mono<T> findById(final ID id);
    Flux<T> findAll();

    Mono<T> save(final T entity);
    Mono<Void> deleteById(final ID id);
}
