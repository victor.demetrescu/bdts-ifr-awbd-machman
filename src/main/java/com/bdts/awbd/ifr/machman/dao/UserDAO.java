package com.bdts.awbd.ifr.machman.dao;

import com.bdts.awbd.ifr.machman.entity.User;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface UserDAO extends BaseDAO, ReactiveSortingRepository<User, Long> {

    Mono<UserDetails> findByUsername(final String username);

    Flux<User> findAllBy();
}
