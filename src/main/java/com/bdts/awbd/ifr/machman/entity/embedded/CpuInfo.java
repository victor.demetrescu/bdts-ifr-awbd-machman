package com.bdts.awbd.ifr.machman.entity.embedded;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.lang.NonNull;

@Data
@Builder
public class CpuInfo {

    @Column("NAME")
    @NonNull
    private String name;

    @Column("SERIAL_CODE")
    @NonNull
    private String serialCode;

    @Column("CORES")
    @NonNull
    private Integer cores;

    @Column("FREQUENCY")
    @NonNull
    private Double frequency;
}
