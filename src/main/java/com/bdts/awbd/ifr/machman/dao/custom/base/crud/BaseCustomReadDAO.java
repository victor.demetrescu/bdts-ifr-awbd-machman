package com.bdts.awbd.ifr.machman.dao.custom.base.crud;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public interface BaseCustomReadDAO extends BaseCustomDAO {

    String PRIMARY_KEY = "ID";

    default Function<Map<String, Object>, ?> getPrimaryKeySelector() {
        return queryResult -> queryResult.get(PRIMARY_KEY);
    }

    default Mono<?> findById(final Long id,
                             final String query,
                             final Function<List<Map<String, Object>>, ?> resultToEntityConversionFunction) {
        return getDatabaseClient()
                .sql(query)
                .bind(PRIMARY_KEY, id)
                .fetch()
                .all()
                .bufferUntilChanged(getPrimaryKeySelector())
                .map(resultToEntityConversionFunction)
                .collectList()
                .map(mono -> mono.get(0));
    }

    default Flux<?> findAll(final String query,
                            final Function<List<Map<String, Object>>, ?> resultToEntityConversionFunction) {
        return getDatabaseClient()
                .sql(query)
                .fetch()
                .all()
                .bufferUntilChanged(getPrimaryKeySelector())
                .map(resultToEntityConversionFunction);
    }
}
