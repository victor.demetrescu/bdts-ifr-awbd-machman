package com.bdts.awbd.ifr.machman.dao.custom.base.crud;

public interface BaseCRUDCustomDAO extends BaseCustomCreateDAO, BaseCustomReadDAO, BaseCustomUpdateDAO, BaseCustomDeleteDAO {
}
