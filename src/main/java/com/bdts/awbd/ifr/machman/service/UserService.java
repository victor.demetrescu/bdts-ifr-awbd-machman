package com.bdts.awbd.ifr.machman.service;

import com.bdts.awbd.ifr.machman.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

    Mono<UserDetails> findByUsername(final String username);
    Flux<User> findAll();
}
