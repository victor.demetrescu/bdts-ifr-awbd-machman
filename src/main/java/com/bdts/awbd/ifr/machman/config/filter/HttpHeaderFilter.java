package com.bdts.awbd.ifr.machman.config.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class HttpHeaderFilter implements WebFilter {

    private static final Logger log = LoggerFactory.getLogger(HttpHeaderFilter.class);

    @Override
    public Mono<Void> filter(final ServerWebExchange serverWebExchange,
                             final WebFilterChain webFilterChain) {
        final List<String> authorizationHeaders = getAuthorizationHeaders(serverWebExchange);
        return checkAuthorizationHeader(authorizationHeaders, serverWebExchange, webFilterChain);
    }

    private List<String> getAuthorizationHeaders(final ServerWebExchange serverWebExchange) {
        return serverWebExchange
                .getRequest()
                .getHeaders()
                .get(HttpHeaders.AUTHORIZATION);

    }

    private Mono<Void> checkAuthorizationHeader(final List<String> authorizationHeaders,
                                                final ServerWebExchange serverWebExchange,
                                                final WebFilterChain webFilterChain) {
        if ((authorizationHeaders == null) || authorizationHeaders.isEmpty()) {
            log.error("{} header not present!", HttpHeaders.AUTHORIZATION);

            final ServerHttpResponse response = serverWebExchange.getResponse();
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        return webFilterChain.filter(serverWebExchange);
    }
}
