package com.bdts.awbd.ifr.machman.entity;

public enum MachineType {
    VIRTUAL,
    PHYSICAL
}
