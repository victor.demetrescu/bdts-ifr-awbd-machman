package com.bdts.awbd.ifr.machman.rest;

import com.bdts.awbd.ifr.machman.entity.Dependency;
import com.bdts.awbd.ifr.machman.service.DependencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/dependency")
@PreAuthorize("isAuthenticated()")
public class DependencyController extends BaseController {

    private final DependencyService dependencyService;

    @Autowired
    public DependencyController(final DependencyService dependencyService) {
        this.dependencyService = dependencyService;
    }

    @GetMapping("/{id}")
    public Mono<Dependency> getDependencyById(@PathVariable("id") final Long id) {
        return dependencyService.findById(id);
    }

    @GetMapping("/all")
    public Flux<Dependency> getAllDependencies() {
        return dependencyService.findAll();
    }

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Dependency> addDependency(@RequestBody final Dependency dependency) {
        return dependencyService.save(dependency);
    }

    @DeleteMapping("/delete/{id}")
    public Mono<Void> deleteLanById(@PathVariable("id") final Long id) {
        return dependencyService.deleteById(id);
    }
}
