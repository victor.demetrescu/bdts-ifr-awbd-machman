package com.bdts.awbd.ifr.machman.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

import java.util.Set;

@Table("DEPENDENCIES")
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Dependency {

    @Id
    @Column("ID")
    private Long id;

    @Column("NAME")
    @NonNull
    private String name;

    @Column("VERSION")
    @NonNull
    private String version;

    @MappedCollection(idColumn = "DEPENDENCY_ID")
    private Set<Machine> machines;

}
