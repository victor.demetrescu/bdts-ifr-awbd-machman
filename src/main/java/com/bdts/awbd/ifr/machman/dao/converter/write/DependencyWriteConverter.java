package com.bdts.awbd.ifr.machman.dao.converter.write;

import com.bdts.awbd.ifr.machman.entity.Dependency;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.r2dbc.core.Parameter;

import static com.bdts.awbd.ifr.machman.dao.converter.write.WriteConverterHelper.addPrimaryKeyToRow;

@WritingConverter
public class DependencyWriteConverter implements Converter<Dependency, OutboundRow> {

    @Override
    public OutboundRow convert(final Dependency source) {
        final OutboundRow row = new OutboundRow();

        addPrimaryKeyToRow(source.getId(), row);
        row.put("NAME", Parameter.from(source.getName()));
        row.put("VERSION", Parameter.from(source.getVersion()));

        return row;
    }
}
