package com.bdts.awbd.ifr.machman.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class MachmanException extends RuntimeException {

    private final String errorMessage;
    private final int statusCode;

    public MachmanException(final String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
        this.statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    public MachmanException(final String errorMessage, final int statusCode) {
        super(errorMessage);
        this.errorMessage = errorMessage;
        this.statusCode = statusCode;
    }


}
