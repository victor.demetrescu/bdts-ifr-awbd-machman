package com.bdts.awbd.ifr.machman.dao;

import com.bdts.awbd.ifr.machman.entity.Cluster;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ClusterDAO extends BaseDAO, ReactiveSortingRepository<Cluster, Long> {

    @Query("SELECT cluster.*, " +
            "lan.id   AS LAN_ID, " +
            "lan.name AS LAN_NAME " +
            "FROM CLUSTERS cluster " +
            "JOIN LANS lan on lan.id = cluster.lan_id ")
    Flux<Cluster> findAllUsingQuery();
}
