package com.bdts.awbd.ifr.machman.dao;

import com.bdts.awbd.ifr.machman.entity.MachineSpecification;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface MachineSpecificationDAO extends BaseDAO, ReactiveSortingRepository<MachineSpecification, Long> {

    String baseQuery = "SELECT ms.*, ";
    String baseFrom = "FROM MACHINE_SPECIFICATIONS ms ";
    String whereClauseById = "WHERE ms.id = :id ";

    String machineQuery = "m.ID AS MACHINE_ID, m.HOSTNAME AS MACHINE_HOSTNAME, m.IP AS MACHINE_IP, " +
            "m.MACHINE_SPECIFICATION_ID AS MACHINE_SPECIFICATION_ID, m.CLUSTER_ID AS MACHINE_CLUSTER_ID ";
    String machineJoin = "JOIN MACHINES m on m.ID = ms.MACHINE_ID ";

    String clusterQuery = "c.ID AS CLUSTER_ID, c.NAME AS CLUSTER_NAME, c.LAN_ID AS CLUSTER_LAN_ID ";
    String clusterJoin = "JOIN CLUSTERS c on c.id = m.CLUSTER_ID ";

    @Query(baseQuery + machineQuery + baseFrom + machineJoin + whereClauseById)
    Mono<MachineSpecification> findByIdWithMachine(final Long id);

    @Query(baseQuery + machineQuery + queryParamsSeparator + clusterQuery + baseFrom + machineJoin + clusterJoin + whereClauseById)
    Mono<MachineSpecification> findByIdWithMachineAndCluster(final Long id);
}
