package com.bdts.awbd.ifr.machman;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;
import reactor.netty.DisposableServer;
import reactor.netty.http.server.HttpServer;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

public class ReactorApplication {

    private static final String BANNER = "\n" +
            "  _____                     _                  _____                                  _____  _                _             _ \n" +
            " |  __ \\                   | |                / ____|                                / ____|| |              | |           | |\n" +
            " | |__) | ___   __ _   ___ | |_  ___   _ __  | (___    ___  _ __ __   __ ___  _ __  | (___  | |_  __ _  _ __ | |_  ___   __| |\n" +
            " |  _  / / _ \\ / _` | / __|| __|/ _ \\ | '__|  \\___ \\  / _ \\| '__|\\ \\ / // _ \\| '__|  \\___ \\ | __|/ _` || '__|| __|/ _ \\ / _` |\n" +
            " | | \\ \\|  __/| (_| || (__ | |_| (_) || |     ____) ||  __/| |    \\ V /|  __/| |     ____) || |_| (_| || |   | |_|  __/| (_| |\n" +
            " |_|  \\_\\\\___| \\__,_| \\___| \\__|\\___/ |_|    |_____/  \\___||_|     \\_/  \\___||_|    |_____/  \\__|\\__,_||_|    \\__|\\___| \\__,_|\n" +
            "                                                                                                                              \n" +
            "                                                                                                                              \n";

    private static final Logger log = LoggerFactory.getLogger(ReactorApplication.class);

    private static DisposableServer reactorServer;

    public static void run() {
        createReactorServer();

        log.info(BANNER);
        log.info("Started reactor server on {}:{}", reactorServer.host(), reactorServer.port());

        reactorServer
                .onDispose()
                .block();
    }

    public static void createReactorServer() {
        final HttpHandler httpHandler = RouterFunctions.toHttpHandler(getReactorRouterFunction());
        final ReactorHttpHandlerAdapter adapter = new ReactorHttpHandlerAdapter(httpHandler);

        final HttpServer httpServer = HttpServer
                .create()
                .host("localhost")
                .port(8200);

        reactorServer = httpServer
                .handle(adapter)
                .bindNow();
    }

    private static RouterFunction<?> getReactorRouterFunction() {
        return route(
                GET("/user/me"),
                getCurrentUser()
        );
    }

    private static HandlerFunction<?> getCurrentUser() {
        return request -> {
            final Mono<String> userNameMono = decodeBasicHTTP(request);
            return ServerResponse
                    .ok()
                    .body(userNameMono, String.class);
        };
    }

    private static Mono<String> decodeBasicHTTP(final ServerRequest request) {
        final String base64Credentials = request
                .headers()
                .firstHeader(HttpHeaders.AUTHORIZATION)
                .substring("Basic".length()).trim();

        byte[] decodedCredentials = Base64
                .getDecoder()
                .decode(base64Credentials);

        final String userName = new String(decodedCredentials, StandardCharsets.UTF_8)
                .split(":", 2)[0];

        return Mono.just(userName);
    }
}
