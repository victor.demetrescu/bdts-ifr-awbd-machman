package com.bdts.awbd.ifr.machman.config;

import java.util.Arrays;
import java.util.List;

public class Roles {

    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";

    public static final List<String> ALL_ROLES = Arrays.asList(ADMIN, USER);

}
