package com.bdts.awbd.ifr.machman.config;

import com.bdts.awbd.ifr.machman.dao.LanDAO;
import io.r2dbc.spi.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer;
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator;
import org.springframework.stereotype.Component;

import static com.bdts.awbd.ifr.machman.config.ApplicationConfiguration.H2_PROFILE;

@Component
public class ApplicationStartupListener implements ApplicationListener<ContextRefreshedEvent> {

    @Value("${database.scripts.create}")
    private String createSQL;

    @Value("${database.scripts.insert}")
    private String insertSQL;

    private static final Logger log = LoggerFactory.getLogger(ApplicationStartupListener.class);

    @Autowired
    private ApplicationConfiguration appConfig;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        final String activeProfile = appConfig.getActiveProfile();
        if (H2_PROFILE.equals(activeProfile)) {
            log.info("Successfully executed {} database scripts.", appConfig.getActiveProfile());
        }
    }

    @Bean
    public ConnectionFactoryInitializer initializer(@Qualifier("connectionFactory") final ConnectionFactory connectionFactory) {
        final ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();

        initializer.setConnectionFactory(connectionFactory);
        initializer.setDatabasePopulator(getDatabaseCreationScript());

        return initializer;
    }

    private ResourceDatabasePopulator getDatabaseCreationScript() {
        if (H2_PROFILE.equals(appConfig.getActiveProfile())) {
            log.info("Initializing {} database.", appConfig.getActiveProfile());
            return new ResourceDatabasePopulator(new ClassPathResource(createSQL), new ClassPathResource(insertSQL));
        }
        return new ResourceDatabasePopulator();
    }

}
