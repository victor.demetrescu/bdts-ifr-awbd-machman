package com.bdts.awbd.ifr.machman.service;

import com.bdts.awbd.ifr.machman.entity.Dependency;

public interface DependencyService extends BaseService<Dependency, Long> {

}
