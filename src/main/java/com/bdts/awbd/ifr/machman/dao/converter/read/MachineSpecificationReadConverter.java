package com.bdts.awbd.ifr.machman.dao.converter.read;

import com.bdts.awbd.ifr.machman.entity.MachineSpecification;
import com.bdts.awbd.ifr.machman.entity.MachineType;
import com.bdts.awbd.ifr.machman.entity.embedded.CpuInfo;
import com.bdts.awbd.ifr.machman.entity.embedded.RamInfo;
import io.r2dbc.spi.Row;
import org.mariadb.r2dbc.MariadbRow;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import static com.bdts.awbd.ifr.machman.dao.converter.read.ReadConverterHelper.getMachine;

@ReadingConverter
public class MachineSpecificationReadConverter implements Converter<Row, MachineSpecification> {

    @Override
    public MachineSpecification convert(final Row source) {
        final MariadbRow row = (MariadbRow) source;

        return MachineSpecification
                .builder()
                .id(row.get("ID", Long.class))
                .type(MachineType.valueOf(row.get("TYPE", String.class)))
                .machineId(row.get("MACHINE_ID", Long.class))
                .ram(getRam(row))
                .cpuInfo(getCpuInfo(row))
                .machine(getMachine(row))
                .build();
    }

    private RamInfo getRam(final MariadbRow row) {
        return RamInfo
                .builder()
                .unit(row.get("RAM_UNIT", String.class))
                .value(row.get("RAM_VALUE", Double.class))
                .build();
    }

    private CpuInfo getCpuInfo(final MariadbRow row) {
        return CpuInfo
                .builder()
                .name(row.get("CPU_NAME", String.class))
                .serialCode(row.get("CPU_SERIAL_CODE", String.class))
                .cores(row.get("CPU_CORES", Integer.class))
                .frequency(row.get("CPU_FREQUENCY", Double.class))
                .build();
    }


}
