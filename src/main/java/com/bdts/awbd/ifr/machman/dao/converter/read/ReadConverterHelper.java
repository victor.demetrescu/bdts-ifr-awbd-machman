package com.bdts.awbd.ifr.machman.dao.converter.read;

import com.bdts.awbd.ifr.machman.entity.*;
import com.bdts.awbd.ifr.machman.entity.embedded.CpuInfo;
import com.bdts.awbd.ifr.machman.entity.embedded.RamInfo;
import org.mariadb.r2dbc.MariadbRow;

import java.util.Collections;

public abstract class ReadConverterHelper {

    public static Lan getLan(final MariadbRow row) {
        if (!hasDataInQuery(row, "LAN_NAME")) {
            return null;
        }

        return Lan
                .builder()
                .id(row.get("LAN_ID", Long.class))
                .name(row.get("LAN_NAME", String.class))
                .clusters(Collections.emptySet())
                .build();
    }

    public static Machine getMachine(final MariadbRow row) {
        if (!hasDataInQuery(row, "MACHINE_HOSTNAME")) {
            return null;
        }

        return Machine
                .builder()
                .id(row.get("MACHINE_ID", Long.class))
                .hostname(row.get("MACHINE_HOSTNAME", String.class))
                .ip(row.get("MACHINE_IP", String.class))
                .specificationId(row.get("MACHINE_SPECIFICATION_ID", Long.class))
                .clusterId(row.get("MACHINE_CLUSTER_ID", Long.class))
                .cluster(getCluster(row))
                .build();
    }

    public static MachineSpecification getMachineSpecification(final MariadbRow row) {
        if (!hasDataInQuery(row, "MACHINE_SPEC_TYPE")) {
            return null;
        }

        return MachineSpecification
                .builder()
                .id(row.get("MACHINE_SPEC_ID", Long.class))
                .type(MachineType.valueOf(row.get("MACHINE_SPEC_TYPE", String.class)))
                .machineId(row.get("MACHINE_ID", Long.class))
                .ram(getRam(row))
                .cpuInfo(getCpuInfo(row))
                .build();
    }

    public static RamInfo getRam(final MariadbRow row) {
        if (!hasDataInQuery(row, "MACHINE_SPEC_RAM_UNIT")) {
            return null;
        }

        return RamInfo
                .builder()
                .unit(row.get("MACHINE_SPEC_RAM_UNIT", String.class))
                .value(row.get("MACHINE_SPEC_RAM_VALUE", Double.class))
                .build();
    }

    public static CpuInfo getCpuInfo(final MariadbRow row) {
        if (!hasDataInQuery(row, "MACHINE_SPEC_CPU_NAME")) {
            return null;
        }

        return CpuInfo
                .builder()
                .name(row.get("MACHINE_SPEC_CPU_NAME", String.class))
                .serialCode(row.get("MACHINE_SPEC_CPU_SERIAL_CODE", String.class))
                .cores(row.get("MACHINE_SPEC_CPU_CORES", Integer.class))
                .frequency(row.get("MACHINE_SPEC_CPU_FREQUENCY", Double.class))
                .build();
    }

    public static Cluster getCluster(final MariadbRow row) {
        if (!hasDataInQuery(row, "CLUSTER_NAME")) {
            return null;
        }

        return Cluster
                .builder()
                .id(row.get("CLUSTER_ID", Long.class))
                .name(row.get("CLUSTER_NAME", String.class))
                .lanId(row.get("CLUSTER_LAN_ID", Long.class))
                .lan(getLan(row))
                .build();
    }

    private static boolean hasDataInQuery(final MariadbRow row,
                                          final String columnName) {
        boolean hasDataInQuery;
        try {
            hasDataInQuery = (row.get(columnName) != null);
        } catch (Exception e) {
            hasDataInQuery = false;
        }

        return hasDataInQuery;
    }
}
