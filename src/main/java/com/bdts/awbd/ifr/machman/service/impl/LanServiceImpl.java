package com.bdts.awbd.ifr.machman.service.impl;

import com.bdts.awbd.ifr.machman.dao.LanDAO;
import com.bdts.awbd.ifr.machman.dao.custom.LanReadDAO;
import com.bdts.awbd.ifr.machman.entity.Lan;
import com.bdts.awbd.ifr.machman.service.LanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class LanServiceImpl implements LanService {

    private final LanReadDAO lanReadDAO;
    private final LanDAO lanDAO;

    @Autowired
    public LanServiceImpl(final LanReadDAO lanReadDAO,
                          final LanDAO lanDAO) {
        this.lanReadDAO = lanReadDAO;
        this.lanDAO = lanDAO;
    }

    @Override
    public Mono<Lan> findById(final Long id) {
        return lanReadDAO.findById(id);
    }

    @Override
    public Flux<Lan> findAll() {
        return lanReadDAO.findAll();
    }

    @Override
    public Mono<Lan> save(final Lan entity) {
        return lanDAO.save(entity);
    }

    @Override
    public Mono<Void> deleteById(final Long id) {
        return lanDAO.deleteById(id);
    }
}
