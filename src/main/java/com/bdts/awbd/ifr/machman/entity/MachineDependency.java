package com.bdts.awbd.ifr.machman.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

@Table("MACHINE_DEPENDENCIES")
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MachineDependency {

    @Id
    @Column("ID")
    private Long id;

    @Column("MACHINE_ID")
    @NonNull
    private Long machineId;

    @Column("DEPENDENCY_ID")
    @NonNull
    private Long dependencyId;

    @Transient
    private Machine machine;

    @Transient
    private Dependency dependency;

}
