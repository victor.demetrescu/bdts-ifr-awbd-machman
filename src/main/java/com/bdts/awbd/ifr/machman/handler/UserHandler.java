package com.bdts.awbd.ifr.machman.handler;

import com.bdts.awbd.ifr.machman.entity.User;
import com.bdts.awbd.ifr.machman.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

@Component
public class UserHandler {

    @Autowired
    private UserService userService;

    public HandlerFunction<ServerResponse> getAllUsers() {
        return
                req -> ServerResponse.ok().body(userService.findAll(), User.class);
    }
}
