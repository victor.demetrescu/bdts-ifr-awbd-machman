package com.bdts.awbd.ifr.machman.service.impl;

import com.bdts.awbd.ifr.machman.dao.DependencyDAO;
import com.bdts.awbd.ifr.machman.dao.custom.DependencyReadDAO;
import com.bdts.awbd.ifr.machman.entity.Dependency;
import com.bdts.awbd.ifr.machman.service.DependencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class DependencyServiceImpl implements DependencyService {

    private final DependencyReadDAO dependencyReadDAO;
    private final DependencyDAO dependencyDAO;

    @Autowired
    public DependencyServiceImpl(final DependencyReadDAO dependencyReadDAO,
                                 final DependencyDAO dependencyDAO) {
        this.dependencyReadDAO = dependencyReadDAO;
        this.dependencyDAO = dependencyDAO;
    }

    @Override
    public Mono<Dependency> findById(final Long id) {
        return dependencyReadDAO.findById(id);
    }

    @Override
    public Flux<Dependency> findAll() {
        return dependencyReadDAO.findAll();
    }

    @Override
    public Mono<Dependency> save(final Dependency entity) {
        return dependencyDAO.save(entity);
    }

    @Override
    public Mono<Void> deleteById(final Long id) {
        return dependencyDAO.deleteById(id);
    }
}
