package com.bdts.awbd.ifr.machman.service;

import com.bdts.awbd.ifr.machman.entity.Cluster;
import reactor.core.publisher.Flux;

public interface ClusterService {

    Flux<Cluster> findAllUsingQuery();
    Flux<Cluster> findAllUsingService();
}
