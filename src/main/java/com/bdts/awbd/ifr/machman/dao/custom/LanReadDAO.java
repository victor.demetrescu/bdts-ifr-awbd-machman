package com.bdts.awbd.ifr.machman.dao.custom;

import com.bdts.awbd.ifr.machman.dao.custom.base.abstracts.AbstractBaseDAO;
import com.bdts.awbd.ifr.machman.entity.*;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class LanReadDAO extends AbstractBaseDAO {

    private static final String ALL_LANs_QUERY = "SELECT " +
            "lan.id               AS ID, " +
            "lan.name             AS LAN_NAME, " +
            "cluster.id           AS CLUSTER_ID, " +
            "cluster.name         AS CLUSTER_NAME, " +
            "cluster.lan_id       AS CLUSTER_LAN_ID, " +
            "machine.id           AS MACHINE_ID, " +
            "machine.hostname     AS MACHINE_HOSTNAME, " +
            "machine.ip           AS MACHINE_IP, " +
            "spec.ID              AS MACHINE_SPEC_ID, " +
            "spec.TYPE            AS MACHINE_SPEC_TYPE, " +
            "spec.RAM_UNIT        AS MACHINE_SPEC_RAM_UNIT, " +
            "spec.RAM_VALUE       AS MACHINE_SPEC_RAM_VALUE, " +
            "spec.CPU_NAME        AS MACHINE_SPEC_CPU_NAME, " +
            "spec.CPU_SERIAL_CODE AS MACHINE_SPEC_CPU_SERIAL_CODE,  " +
            "spec.CPU_CORES       AS MACHINE_SPEC_CPU_CORES, " +
            "spec.CPU_FREQUENCY   AS MACHINE_SPEC_CPU_FREQUENCY " +
            "FROM LANS lan " +
            "LEFT JOIN CLUSTERS               cluster ON lan.id = cluster.lan_id " +
            "LEFT JOIN MACHINES               machine ON machine.cluster_id = cluster.id " +
            "LEFT JOIN MACHINE_SPECIFICATIONS spec    ON spec.id = machine.machine_specification_id ";
    private static final String LAN_BY_ID_QUERY = ALL_LANs_QUERY + "WHERE lan.id = :ID ";
    private static final String ALL_LANs_FULL_QUERY = ALL_LANs_QUERY + ORDER_BY_PRIMARY_KEY;

    @SuppressWarnings("unchecked")
    public Mono<Lan> findById(final Long id) {
        return (Mono<Lan>) findById(id, LAN_BY_ID_QUERY, this::convertEntityFromResults);
    }

    @SuppressWarnings("unchecked")
    public Flux<Lan> findAll() {
        return (Flux<Lan>) findAll(ALL_LANs_FULL_QUERY, this::convertEntityFromResults);
    }

    private Lan convertEntityFromResults(final List<Map<String, Object>> results) {
        final Long lanId = getPrimaryKey(results);
        return Lan
                .builder()
                .id(lanId)
                .name((String) getBaseProperty(results, "LAN_NAME"))
                .clusters(getClustersFromResults(results, lanId))
                .build();
    }

    private Set<Cluster> getClustersFromResults(final List<Map<String, Object>> results,
                                                final Long lanId) {
        final Map<Cluster, Set<Machine>> clusterMachineMap = new HashMap<>();

        results
                .stream()
                .map(this::getClusterMachineEntryFromRow)
                .filter(entry -> entry.getKey().getId() != null)
                .forEach(clusterMachineEntry -> {
                    clusterMachineMap.computeIfAbsent(clusterMachineEntry.getKey(), cluster -> {
                        final Set<Machine> machinesForCluster = new HashSet<>();
                        machinesForCluster.add(clusterMachineEntry.getValue());
                        return machinesForCluster;
                    });
                    clusterMachineMap.computeIfPresent(clusterMachineEntry.getKey(), (cluster, machines) -> {
                        machines.add(clusterMachineEntry.getValue());
                        return machines;
                    });
                });

        return clusterMachineMap
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().getLanId().equals(lanId))
                .map(entry -> {
                    final Cluster cluster = entry.getKey();
                    cluster.setMachines(entry.getValue());
                    return cluster;
                })
                .collect(Collectors.toSet());
    }

    private Map.Entry<Cluster, Machine> getClusterMachineEntryFromRow(final Map<String, Object> row) {
        final Cluster cluster = getClusterFromRow(row);
        final Machine machine = getMachineFromRow(row, false);

        return Map.entry(cluster, machine);
    }
}
