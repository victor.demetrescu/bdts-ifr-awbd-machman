package com.bdts.awbd.ifr.machman.config;

import com.bdts.awbd.ifr.machman.dao.converter.read.ClusterReadConverter;
import com.bdts.awbd.ifr.machman.dao.converter.write.DependencyWriteConverter;
import com.bdts.awbd.ifr.machman.dao.converter.read.MachineReadConverter;
import com.bdts.awbd.ifr.machman.dao.converter.read.MachineSpecificationReadConverter;
import com.bdts.awbd.ifr.machman.dao.converter.write.LanWriteConverter;
import com.bdts.awbd.ifr.machman.exception.MachmanException;
import io.r2dbc.h2.H2ConnectionConfiguration;
import io.r2dbc.h2.H2ConnectionFactory;
import io.r2dbc.h2.H2ConnectionOption;
import io.r2dbc.spi.ConnectionFactory;
import org.mariadb.r2dbc.MariadbConnectionConfiguration;
import org.mariadb.r2dbc.MariadbConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.bdts.awbd.ifr.machman.config.ApplicationConfiguration.H2_PROFILE;
import static com.bdts.awbd.ifr.machman.config.ApplicationConfiguration.MARIADB_PROFILE;

@Configuration
@EnableR2dbcRepositories
public class DatabaseConfig extends AbstractR2dbcConfiguration {

    private static final Logger log = LoggerFactory.getLogger(DatabaseConfig.class);

    private static final String MACHMAN = "machman";

    @Autowired
    private ApplicationConfiguration appConfig;

    @Override
//    @Bean
    public ConnectionFactory connectionFactory() {
        final String activeProfile = appConfig.getActiveProfile();
        ConnectionFactory connectionFactory = null;

        if (MARIADB_PROFILE.equals(activeProfile)) {
            connectionFactory = new MariadbConnectionFactory(mariadbConnectionConfiguration());
        } else if (H2_PROFILE.equals(activeProfile)) {
            connectionFactory = new H2ConnectionFactory(h2ConnectionConfiguration());
        }

        if (connectionFactory != null) {
            log.info("Returning {} for profile {}", connectionFactory.getClass().getSimpleName(), activeProfile);
            return connectionFactory;
        }

        throw new MachmanException("Unknown profile specified: " + activeProfile);
    }

    @Override
    protected List<Object> getCustomConverters() {
       return Stream
               .concat(getReadConverters(), getWriteConverters())
               .collect(Collectors.toList());
    }

    private MariadbConnectionConfiguration mariadbConnectionConfiguration() {
        return MariadbConnectionConfiguration
                .builder()
                .host("localhost")
                .port(MariadbConnectionConfiguration.DEFAULT_PORT)
                .username(MACHMAN)
                .password(MACHMAN)
                .database(MACHMAN)
                .build();
    }

    private H2ConnectionConfiguration h2ConnectionConfiguration() {
        return H2ConnectionConfiguration
                .builder()
                .inMemory(MACHMAN)
                .property(H2ConnectionOption.DB_CLOSE_DELAY,  "-1")
                .build();
    }

    private Stream<Object> getReadConverters() {
        return Stream.of(
                new ClusterReadConverter(),
                new MachineReadConverter(),
                new MachineSpecificationReadConverter()
        );
    }

    private Stream<Object> getWriteConverters() {
        return Stream.of(
                new DependencyWriteConverter(),
                new LanWriteConverter()
        );
    }
}
