package com.bdts.awbd.ifr.machman.service.impl;

import com.bdts.awbd.ifr.machman.dao.UserDAO;
import com.bdts.awbd.ifr.machman.entity.User;
import com.bdts.awbd.ifr.machman.exception.UserNotFoundException;
import com.bdts.awbd.ifr.machman.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserDAO userDAO;

    @Autowired
    public UserServiceImpl(final UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public Mono<UserDetails> findByUsername(final String username) {
        return userDAO
                .findByUsername(username)
                .switchIfEmpty(handleUserNotFoundError(username, null))
                .onErrorResume(error -> handleUserNotFoundError(username, error));
    }

    @Override
    public Flux<User> findAll() {
        return userDAO.findAllBy();
    }

    private Mono<UserDetails> handleUserNotFoundError(final String username,
                                                      final Throwable error) {
        log.error(UserNotFoundException.USER_NOT_FOUND_ERROR_MESSAGE + username);
        log.debug(UserNotFoundException.USER_NOT_FOUND_ERROR_MESSAGE + username, error);
        return Mono.error(new UserNotFoundException(username));
    }
}
