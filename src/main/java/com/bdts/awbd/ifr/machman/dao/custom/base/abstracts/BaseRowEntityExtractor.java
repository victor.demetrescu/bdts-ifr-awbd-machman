package com.bdts.awbd.ifr.machman.dao.custom.base.abstracts;

import com.bdts.awbd.ifr.machman.entity.Cluster;
import com.bdts.awbd.ifr.machman.entity.Machine;
import com.bdts.awbd.ifr.machman.entity.MachineSpecification;
import com.bdts.awbd.ifr.machman.entity.MachineType;
import com.bdts.awbd.ifr.machman.entity.embedded.CpuInfo;
import com.bdts.awbd.ifr.machman.entity.embedded.RamInfo;

import java.util.Map;

public abstract class BaseRowEntityExtractor {

    protected Cluster getClusterFromRow(final Map<String, Object> row) {
        final Long clusterId = (Long) row.get("CLUSTER_ID");
        if (clusterId == null) {
            return null;
        }

        return Cluster
                .builder()
                .id(clusterId)
                .name((String) row.get("CLUSTER_NAME"))
                .lanId((Long) row.get("CLUSTER_LAN_ID"))
                .build();
    }

    protected Machine getMachineFromRow(final Map<String, Object> row,
                                        final boolean withCluster) {
        final Long machineId = (Long) row.get("MACHINE_ID");
        if (machineId == null) {
            return null;
        }

        return Machine
                .builder()
                .id(machineId)
                .hostname((String) row.get("MACHINE_HOSTNAME"))
                .ip((String) row.get("MACHINE_IP"))
                .specificationId((Long) row.get("MACHINE_SPEC_ID"))
                .clusterId((Long) row.get("CLUSTER_ID"))
                .machineSpecification(getMachineSpecificationFromRow(row, false))
                .cluster(withCluster ? getClusterFromRow(row) : null)
                .build();
    }

    protected MachineSpecification getMachineSpecificationFromRow(final Map<String, Object> row,
                                                                  final boolean withMachine) {
        final String machineSpecificationType = (String) row.get("MACHINE_SPEC_TYPE");
        if (machineSpecificationType == null) {
            return null;
        }

        return MachineSpecification
                .builder()
                .id((Long) row.get("MACHINE_SPEC_ID"))
                .type(MachineType.valueOf((String) row.get("MACHINE_SPEC_TYPE")))
                .machineId((Long) row.get("MACHINE_ID"))
                .ram(getRamFromRow(row))
                .cpuInfo(getCpuInfoFromRow(row))
                .machine(withMachine ? getMachineFromRow(row, false) : null)
                .build();
    }

    protected static RamInfo getRamFromRow(final Map<String, Object> row) {
        return RamInfo
                .builder()
                .unit((String) row.get("MACHINE_SPEC_RAM_UNIT"))
                .value((Double) row.get("MACHINE_SPEC_RAM_VALUE"))
                .build();
    }

    protected static CpuInfo getCpuInfoFromRow(final Map<String, Object> row) {
        return CpuInfo
                .builder()
                .name((String) row.get("MACHINE_SPEC_CPU_NAME"))
                .serialCode((String) row.get("MACHINE_SPEC_CPU_SERIAL_CODE"))
                .cores((Integer) row.get("MACHINE_SPEC_CPU_CORES"))
                .frequency((Double) row.get("MACHINE_SPEC_CPU_FREQUENCY"))
                .build();
    }
}
