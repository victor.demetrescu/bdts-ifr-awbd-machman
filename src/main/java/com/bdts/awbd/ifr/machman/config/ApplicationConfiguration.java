package com.bdts.awbd.ifr.machman.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfiguration {

    public static final String MARIADB_PROFILE = "mariadb";
    public static final String H2_PROFILE = "h2";

    @Autowired
    private Environment environment;

    public String getActiveProfile() {
        return environment.getActiveProfiles()[0];
    }
}
