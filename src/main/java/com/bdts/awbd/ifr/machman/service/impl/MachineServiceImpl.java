package com.bdts.awbd.ifr.machman.service.impl;

import com.bdts.awbd.ifr.machman.dao.MachineDAO;
import com.bdts.awbd.ifr.machman.dao.MachineSpecificationDAO;
import com.bdts.awbd.ifr.machman.entity.Machine;
import com.bdts.awbd.ifr.machman.entity.MachineSpecification;
import com.bdts.awbd.ifr.machman.service.MachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class MachineServiceImpl implements MachineService {

    private final MachineDAO machineDAO;
    private final MachineSpecificationDAO machineSpecificationDAO;

    @Autowired
    public MachineServiceImpl(final MachineDAO machineDAO,
                              final MachineSpecificationDAO machineSpecificationDAO) {
        this.machineDAO = machineDAO;
        this.machineSpecificationDAO = machineSpecificationDAO;
    }

    @Override
    public Mono<Machine> getMachineById(final Long id,
                                        final boolean withCluster,
                                        final boolean withMachineSpecification) {
        if (withCluster) {
            if (withMachineSpecification) {
                return machineDAO.findByIdWithClusterAndMachineSpecification(id);
            }
            return machineDAO.findByIdWithCluster(id);
        }
        return machineDAO.findById(id);
    }

    @Override
    public Mono<MachineSpecification> getMachineSpecificationById(final Long id,
                                                                  final boolean withMachine,
                                                                  final boolean withCluster) {
        if (withMachine) {
            if (withCluster) {
                return machineSpecificationDAO.findByIdWithMachineAndCluster(id);
            }
            return machineSpecificationDAO.findByIdWithMachine(id);
        }
        return machineSpecificationDAO.findById(id);
    }
}
