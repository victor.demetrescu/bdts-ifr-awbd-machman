package com.bdts.awbd.ifr.machman.exception;

public class UserNotFoundException extends MachmanException {

    public static final String USER_NOT_FOUND_ERROR_MESSAGE = "Failed to find user: ";

    public UserNotFoundException(final String username) {
        super(USER_NOT_FOUND_ERROR_MESSAGE + username);
    }
}
