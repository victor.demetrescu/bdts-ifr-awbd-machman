package com.bdts.awbd.ifr.machman.service;

import com.bdts.awbd.ifr.machman.entity.Machine;
import com.bdts.awbd.ifr.machman.entity.MachineSpecification;
import reactor.core.publisher.Mono;

public interface MachineService {

    Mono<Machine> getMachineById(final Long id,
                                 final boolean withCluster,
                                 final boolean withMachineSpecification);

    Mono<MachineSpecification> getMachineSpecificationById(final Long id,
                                                           final boolean withMachine,
                                                           final boolean withCluster);
}
