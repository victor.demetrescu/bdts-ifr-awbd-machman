package com.bdts.awbd.ifr.machman.service;

import com.bdts.awbd.ifr.machman.entity.Lan;

public interface LanService extends BaseService<Lan, Long> {
}
