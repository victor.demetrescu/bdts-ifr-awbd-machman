package com.bdts.awbd.ifr.machman.rest;

import com.bdts.awbd.ifr.machman.exception.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import reactor.core.publisher.Mono;

public class BaseController {

    private static final Logger log = LoggerFactory.getLogger(BaseController.class);
    private static final String HANDLING_ERROR_LOG = "Handling error to response: {}";

    @ExceptionHandler
    public Mono<ErrorResponse> handleException(final Throwable exception) {
        log.error(HANDLING_ERROR_LOG, exception.getMessage());
        log.debug("Stack trace: ", exception);
        return Mono.just(ErrorResponse.fromException(exception));
    }
}
