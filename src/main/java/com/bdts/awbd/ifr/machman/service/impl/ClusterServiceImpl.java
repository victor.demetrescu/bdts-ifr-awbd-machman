package com.bdts.awbd.ifr.machman.service.impl;

import com.bdts.awbd.ifr.machman.dao.ClusterDAO;
import com.bdts.awbd.ifr.machman.entity.Cluster;
import com.bdts.awbd.ifr.machman.entity.Lan;
import com.bdts.awbd.ifr.machman.service.ClusterService;
import com.bdts.awbd.ifr.machman.service.LanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class ClusterServiceImpl implements ClusterService {

    private final ClusterDAO clusterDAO;
    private final LanService lanService;

    @Autowired
    public ClusterServiceImpl(final ClusterDAO clusterDAO,
                              final LanService lanService) {
        this.clusterDAO = clusterDAO;
        this.lanService = lanService;
    }

    @Override
    public Flux<Cluster> findAllUsingQuery() {
        return clusterDAO.findAllUsingQuery();
    }

    @Override
    public Flux<Cluster> findAllUsingService() {
        final Flux<Lan> allLANs = lanService.findAll();
        final Flux<Cluster> allClusters = clusterDAO.findAll();

        return allClusters.zipWith(
                allLANs,
                (cluster, lan) -> {
                    if (cluster.getLanId().equals(lan.getId())) {
                        cluster.setLan(lan);
                    }
                    return cluster;
                });
    }
}
