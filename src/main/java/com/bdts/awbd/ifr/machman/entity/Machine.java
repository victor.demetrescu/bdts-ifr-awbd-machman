package com.bdts.awbd.ifr.machman.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.NonNull;

import java.util.Set;

@Table("MACHINES")
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Machine {

    @Id
    @Column("ID")
    private Long id;

    @Column("HOSTNAME")
    @NonNull
    private String hostname;

    @Column("IP")
    @NonNull
    private String ip;

    @Column("CLUSTER_ID")
    @NonNull
    @JsonIgnore
    private Long clusterId;

    @Column("MACHINE_SPECIFICATION_ID")
    @NonNull
    @JsonIgnore
    private Long specificationId;

    @MappedCollection(idColumn = "MACHINE_ID")
    private Set<Dependency> dependencies;

    @Transient
    private Cluster cluster;

    @Transient
    private MachineSpecification machineSpecification;

}
