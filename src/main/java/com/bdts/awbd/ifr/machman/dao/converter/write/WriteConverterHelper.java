package com.bdts.awbd.ifr.machman.dao.converter.write;

import com.bdts.awbd.ifr.machman.entity.*;
import com.bdts.awbd.ifr.machman.entity.embedded.CpuInfo;
import com.bdts.awbd.ifr.machman.entity.embedded.RamInfo;
import org.mariadb.r2dbc.MariadbRow;
import org.springframework.data.r2dbc.mapping.OutboundRow;
import org.springframework.r2dbc.core.Parameter;

import java.util.Collections;

public abstract class WriteConverterHelper {

    private static final String PRIMARY_KEY = "ID";

    public static void addPrimaryKeyToRow(final Object primaryKey,
                                          final OutboundRow row) {
        if (primaryKey != null) {
            row.put(PRIMARY_KEY, Parameter.from(primaryKey));
        }
    }
}
