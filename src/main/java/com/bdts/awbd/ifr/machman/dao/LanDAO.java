package com.bdts.awbd.ifr.machman.dao;

import com.bdts.awbd.ifr.machman.entity.Lan;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanDAO extends ReactiveSortingRepository<Lan, Long> {
}
