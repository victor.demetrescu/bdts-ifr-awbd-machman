package com.bdts.awbd.ifr.machman.dao.custom.base.abstracts;

import com.bdts.awbd.ifr.machman.dao.custom.base.crud.BaseCRUDCustomDAO;
import com.bdts.awbd.ifr.machman.exception.MachmanException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.r2dbc.core.DatabaseClient;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

public abstract class AbstractBaseDAO extends BaseRowEntityExtractor implements BaseCRUDCustomDAO {

    private static final String PRIMARY_KEY = "ID";

    protected static final String ORDER_BY_PRIMARY_KEY = "ORDER BY ID ";

    @Autowired
    private DatabaseClient databaseClient;

    @Override
    public DatabaseClient getDatabaseClient() {
        return this.databaseClient;
    }

    public Function<Map<String, Object>, ?> getPrimaryKeySelector() {
        return queryResult -> queryResult.get(PRIMARY_KEY);
    }

    protected Long getPrimaryKey(final List<Map<String, Object>> queryResults) {
        return (Long) getBaseProperty(queryResults, PRIMARY_KEY);
    }

    protected Object getBaseProperty(final List<Map<String, Object>> queryResults,
                                     final String basePropertyName) {
        final Map<String, Object> firstEntryColumnValues = queryResults
                .stream()
                .findFirst()
                .orElseThrow(() -> new MachmanException("Query returned no results"));

        return firstEntryColumnValues.getOrDefault(basePropertyName, getDefaultValue(firstEntryColumnValues, basePropertyName));
    }

    protected Object getDefaultValue(final Map<String, Object> firstEntryColumnValues,
                                     final String basePropertyName) {
        final Set<String> queryColumns = firstEntryColumnValues.keySet();
        final boolean columnIsInQueryResults = queryColumns.contains(basePropertyName);

        if (!columnIsInQueryResults) {
            throw new MachmanException(String.format("%s is not in query results: %s", basePropertyName, Arrays.toString(queryColumns.toArray())));
        }
        return null;
    }

}
